<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('register', 'AuthController@register');
    Route::post('update_user', 'AuthController@update_user');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('profile', 'AuthController@profile');
    Route::post('delete', 'AuthController@delete');
    
    Route::post('addUpdatebook', 'BookManagementController@addUpdatebook');
    Route::post('getbook', 'BookManagementController@getbook');
    Route::post('deletebook', 'BookManagementController@delete');
    Route::post('bookentry', 'BookManagementController@bookentry');

});
