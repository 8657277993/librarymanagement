<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class BookRecord extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'book_records';
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
    protected $primaryKey = 'id';
    public $field;
    public $timestamps = true;

    
    
     public function addBookRecord() {
        $arrResp = [];
        $status = false;
        $message = '';
        $addData = array();
        $lastInsertId = 0;
        try {
            $inputData = $this->field['data'];
            $bookRecordObj = new BookRecord();
            $addData['user_id'] = $inputData['user_id'];
            $addData['book_id'] = $inputData['book_id'];
            $addData['book_type'] = $inputData['book_type'];
            $addData->created_at = $bookRecordObj->freshTimestamp();
            $addData->updated_at = $bookRecordObj->freshTimestamp();
            $lastInsertId = $bookRecordObj->insertGetId($addData);
            if ($lastInsertId) {
                $status = true;
                $message = 'Record added succsessfully';
            } else {
                $message = 'failed';
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }
    
    
}
