<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Hash;
use DB;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login() {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token) {
        return response()->json([
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function register(Request $request) {
        $responseArr = null;
        $status = false;
        $input = $request->all();
        //  Validation part
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'mobile' => 'required|unique:users,mobile',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'city' => 'required',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        DB::beginTransaction();
        try {

            $user = new User();
            $hashedPassword = Hash::make($input['password']);
            $input['password'] = $hashedPassword;
            $user->field['data'] = $input;
            $response = $user->register();
            $message = $response['message'];
            if ($response['status']) {
                DB::commit();
                $status = true;
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        $responseArr['status'] = $status;
        $responseArr['message'] = $message;
        echo json_encode($responseArr);
        exit;
    }

    public function update_user(Request $request) {
        $responseArr = null;
        $status = false;
        $input = $request->all();
        $input['id'] = auth()->user()->u_id;
        //  Validation part
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'mobile' => 'required|unique:users,mobile,' . $input['id'] . ',u_id',
            'email' => 'required|unique:users,email,' . $input['id'] . ',u_id',
            'age' => 'required',
            'gender' => 'required',
            'city' => 'required',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        DB::beginTransaction();
        try {

            $user = new User();
            if (isset($input['password']) && !empty($input['password'])) {
                $hashedPassword = Hash::make($input['password']);
                $input['password'] = $hashedPassword;
            }
            $user->field['data'] = $input;
            $response = $user->register();
            $message = $response['message'];
            if ($response['status']) {
                DB::commit();
                $status = true;
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        $responseArr['status'] = $status;
        $responseArr['message'] = $message;
        echo json_encode($responseArr);
        exit;
    }

    public function profile() {
        $status = true;
        $responseArr['status'] = $status;
        $responseArr['message'] = 'User record';
        $responseArr['data'] = auth()->user();
        echo json_encode($responseArr);
        exit;
    }

    public function delete() {
        $status = false;
        $message = 'Unable to delete record';
        DB::beginTransaction();
        try {
            $response = User::where('u_id', '=', auth()->user()->u_id)->delete();
            if ($response) {
                DB::commit();
                $status = true;
                $message = 'Record deleted successfully';
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }
        $responseArr['status'] = $status;
        $responseArr['message'] = $message;
        echo json_encode($responseArr);
        exit;
    }

}
