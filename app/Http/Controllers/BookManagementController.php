<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookRecord;
use Illuminate\Http\Request;
use Hash;
use DB;
use File;
use Illuminate\Support\Facades\Validator;

class BookManagementController extends Controller {

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('jwt', ['except' => ['login', 'register']]);
    }

    public function addUpdatebook(Request $request) {
        $responseArr = null;
        $status = false;
        $message = '';
        $input = $request->all();
        //  Validation part
        $rules = [
            'book_name' => 'required|unique:books,book_name,' . $input['id'] . ',b_id',
            'author' => 'required',
            'cover_image' => 'required',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        DB::beginTransaction();
        try {
            $i = 1;
            $input['cover_image'] = '';
            $fileName = '';
            if (!empty($request->file('cover_image'))) {
                $image = $request->file('cover_image');
                $fileName = $image->getClientOriginalName();
            }
            
            $input['cover_image'] = $fileName;
            $user = new Book();
            $user->field['data'] = $input;
            $response = $user->addUpdatebook();
            $message = $response['message'];
            
            if ($response['status']) {
                DB::commit();
                $original = public_path() . '/images/' . 'original';
                $image->move($original, $fileName);
                $status = true;
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }

        $responseArr['status'] = $status;
        $responseArr['message'] = $message;
        echo json_encode($responseArr);
        exit;
    }

    public function getbook(Request $request) {
        $input = $request->all();
        //  Validation part
        $rules = [
            'id' => 'required',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        $record = Book::where('b_id', $request->id)->first();
        $responseArr['status'] = true;
        $responseArr['message'] = 'Record fetch successfully';
        $responseArr['data'] = $record;
        echo json_encode($responseArr);
        exit;
    }

    public function delete(Request $request) {
        $status = false;
        $message = 'Unable to delete record';
        DB::beginTransaction();
        try {
            $response = Book::where('b_id', '=', $request->id)->delete();
            if ($response) {
                DB::commit();
                $status = true;
                $message = 'Record deleted successfully';
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }
        $responseArr['status'] = $status;
        $responseArr['message'] = $message;
        echo json_encode($responseArr);
        exit;
    }
    
    public function bookentry(Request $request) {
        $status = false;
        $input = $request->all();
        //  Validation part
        $rules = [
            'book_id' => 'required',
            'book_type' => 'required',
        ];
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        DB::beginTransaction();
        try {
            $input['user_id'] = auth()->user()->u_id;
            $bookRecord = new BookRecord();
            $bookRecord->field['data'] = $input;
            $response = $bookRecord->addBookRecord();
             $message = $response['message'];
            if ($response) {
                DB::commit();
                $status = true;
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
        }
        $responseArr['status'] = $status;
        $responseArr['message'] = $message;
        echo json_encode($responseArr);
        exit;
    }

}
