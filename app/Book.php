<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'books';
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
    protected $primaryKey = 'b_id';
    public $field;

    
    
     public function addUpdatebook() {
        $arrResp = [];
        $status = false;
        $message = '';
        $addData = array();
        $lastInsertId = 0;
        try {
            $inputData = $this->field['data'];
            $bookObj = new Book();
//                    echo '<pre>'; print_r($inputData); exit('in c model');
            $addData['b_id'] = $inputData['id'];
            $addData['book_name'] = $inputData['book_name'];
            $addData['author'] = $inputData['author'];
            $addData['cover_image'] = $inputData['cover_image'];
            if (!empty($inputData['id'])) {
                $query = self::query();
                $query->where('b_id', $inputData['id']);
                $lastInsertId = $query->update($addData);
                $lastInsertId = $inputData['id'];
                $message = 'Update successfully';
            } else {
                $lastInsertId = $bookObj->insertGetId($addData);
                $message = 'Added successfully';
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                $status = false;
                if (!empty($inputData['id'])) {
                    $message = 'Unalbe Update record';
                } else {
                   $message = 'Unalbe add record';
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }
    
    
}
