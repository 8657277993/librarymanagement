<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = [
//        'name', 'email', 'password',
//    ];
    protected $primaryKey = 'u_id';
    public $field;
   
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    
    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    
  
    
     public function register() {
        $arrResp = [];
        $status = false;
        $message = '';
        $addData = array();
        $lastInsertId = 0;
        try {
            $inputData = $this->field['data'];
            $userObj = new User();
            if(isset($inputData['id']) && !empty($inputData['id'])){
                $addData['u_id'] = $inputData['id'];
            }
            $addData['firstname'] = $inputData['firstname'];
            $addData['lastname'] = $inputData['lastname'];
            $addData['mobile'] = $inputData['mobile'];
            $addData['email'] = $inputData['email'];
            if(isset($inputData['password']) && !empty($inputData['password'])){
                $addData['password'] = $inputData['password'];
            }
            $addData['age'] = $inputData['age'];
            $addData['gender'] = $inputData['gender'];
            $addData['city'] = $inputData['city'];
           
            if (isset($inputData['id']) && !empty($inputData['id'])) {
                $query = self::query();
                $query->where('u_id', $inputData['id']);
                $lastInsertId = $query->update($addData);
                $lastInsertId = $inputData['id'];
                $message = 'Update successfully';
            } else {
                $lastInsertId = $userObj->insertGetId($addData);
                $message = 'Added successfully';
            }
            if ($lastInsertId) {
                $status = true;
            } else {
                $status = false;
                if (!empty($inputData['id'])) {
                    $message = 'Unalbe Update record';
                } else {
                    $message = 'Unalbe add record';
                }
            }
        } catch (\Exception $ex) {
            $status = false;
            $message = $ex->getMessage();
        }
        $arrResp['status'] = $status;
        $arrResp['message'] = $message;
        $arrResp['lastInsertId'] = $lastInsertId;
        return $arrResp;
    }
    
    
}
